I dag er 13. februar, dag 30/16 av mitt Kina-opphold.
Vi var utendørs to ganger i dag, først for å handle, så for å hente en laptop som forhåpentligvis har blitt fikset nå. Vi handlet i en passe stor butikk (etter norske standarder). De hadde et stort utvalg av kjøtt og frukt. Ost hadde de ikke, med mindre en regner med osten i en porsjonspakning med frossenpizza. Utvalget av brød var begrenset til porsjonspakninger med loff med eller uten fyll.

Dag 31/16
I dag har SAS forlenget kanselleringen av fly til og fra Kina, for tredje gang. Våre billetter som ble utsatt fra 30. jan til 10. feb, og igjen til 16. mars, blir utsatt til en gang i april. Mon tro om bookingselskapet vi kjøpte billetter gjennom vil være like kranglete når de tar kontakt med oss igjen som før.

Dag 32/16
I dag lagde vi 糯米圆子/kleberisboller igjen. Jeg fikk 粑粑/"baba" (seige riskarbonader laget av kleberis) med sukker til frokost. Hongying fikk også tak i to typer 'brød' til meg på butikken, den ene var for søt for min smak, den andre er visst fullkorns'brød', det lover jo godt, men pakken sier det er søtt brød det også, vi får se når jeg smaker det i morgen. Kanskje jeg skal lage mer peanøttsmør til å ha på? Jeg gleder meg egentlig mest til å spise frokostblandingen jeg kjøpte i forrigårs.

Dag 33/16
Frokostblandingen var god. Fullkorns'brødet' var søtt, minnet mer om kake, men med peanøttsmøret jeg lagde var det godt. Tror peanøttsmøret vil smake bedre på hjemmebakt brød. Så lagde vi 包子/"Baozi" (en type kinesisk fyllt brød), det gleder jeg meg til å ha i dagene fremover. Vi ble kontaktet av bookingselskapet, som spurte om vi ville bytte til det neste sas-flyet, eller om vi ville fly samme dag (16. mars) på et annet flyselskap.

Dag 34/16
I dag kjøpte Hongying kinasjakk, så vi spilte det en del ganger. Også hang vi opp et hoppetau på takterrassen så vi kunne spille badminton med poeng. Insektene er på vei tilbake nå, håper ingen av dem kommer med den sykdommen jeg ikke vaksinerte meg mot, siden jeg egentlig skulle være hjemme igjen før insektene kom tilbake. Så lagde vi 葱油饼/"Cong you bing" (kinesiske pannekaker med grønn løk inni), det var godt. Bortsett fra 'brødet' deres, som etter min smak er alt for søtt, tror jeg jeg liker alt de har her som er laget av hvete. Jeg blir også stadig overrasket over hvor mye bedre tofuen her er enn den en får tak i i Norge.
